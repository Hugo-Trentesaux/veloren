use super::Message;
use crate::ui::{
    fonts::IcedFonts as Fonts,
    ice::{component::neat_button, style, Element},
};
use i18n::Localization;
use iced::{button, Column, Container, HorizontalAlignment, Length, Space};

pub struct Screen {
    back_button: button::State,
}

impl Screen {
    pub fn new() -> Self {
        Self {
            back_button: Default::default(),
        }
    }

    pub(super) fn view(
        &mut self,
        // global_state: &GlobalState,
        fonts: &Fonts,
        // imgs: &Imgs,
        i18n: &Localization,
        button_style: style::button::Style,
    ) -> Element<Message> {
        // // tells to show wettings window
        // let show = crate::hud::Show {
        //     ui: false,
        //     intro: false,
        //     help: false,
        //     crafting: false,
        //     bag: false,
        //     bag_inv: false,
        //     bag_details: false,
        //     trade: false,
        //     trade_details: false,
        //     social: false,
        //     diary: false,
        //     group: false,
        //     quest: false,
        //     group_menu: false,
        //     esc_menu: false,
        //     open_windows: crate::hud::Windows::Settings, // <-- here
        //     map: false,
        //     ingame: false,
        //     chat_tab_settings_index: None,
        //     settings_tab: crate::hud::SettingsTab::Interface,
        //     diary_fields: crate::hud::diary::DiaryShow::default(),
        //     crafting_fields: crate::hud::crafting::CraftingShow::default(),
        //     social_search_key: None,
        //     want_grab: false,
        //     stats: false,
        //     free_look: false,
        //     auto_walk: false,
        //     zoom_lock: crate::hud::ChangeNotification::default(),
        //     camera_clamp: false,
        //     prompt_dialog: None,
        //     location_markers: crate::hud::ChangeNotification::MapMarkers::default(),
        //     trade_amount_input_key: None,
        // }

        // let settings_window = SettingsWindow::new(
        //     global_state,
        //     show,
        //     imgs,
        //     fonts,
        //     i18n,
        //     None,
        //     0f32,
        // )

        Container::new(
            Container::new(
                Column::with_children(vec![
                    iced::Text::new(i18n.get_msg("main-settings"))
                        .font(fonts.alkhemi.id)
                        .size(fonts.alkhemi.scale(35))
                        .width(Length::Fill)
                        .horizontal_alignment(HorizontalAlignment::Center)
                        .into(),
                    Space::new(Length::Fill, Length::Units(25)).into(),
                    Container::new(
                        Container::new(neat_button(
                            &mut self.back_button,
                            i18n.get_msg("common-back"),
                            0.7,
                            button_style,
                            Some(Message::Back),
                        ))
                        .height(Length::Units(fonts.cyri.scale(50))),
                    )
                    .center_x()
                    .height(Length::Shrink)
                    .width(Length::Fill)
                    .into(),
                ])
                .spacing(5)
                .padding(20)
                .width(Length::Fill)
                .height(Length::Fill),
            )
            .style(
                style::container::Style::color_with_double_cornerless_border(
                    (22, 19, 17, 255).into(),
                    (11, 11, 11, 255).into(),
                    (54, 46, 38, 255).into(),
                ),
            ),
        )
        .center_x()
        .center_y()
        .padding(70)
        .width(Length::Fill)
        .height(Length::Fill)
        .into()
    }
}
